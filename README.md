# PHP7 / MySQL8 dev env

Quick dev env with PHP7 / MySQL8 (https)

## Setup

Rename .env.example file to .env and edit it.

### HTTPS Certificate
Generate certificate files : 
 - `.docker/https/cert.pem`
 - `.docker/https/key.pem`
 
Self-sign certificate command:  
`openssl req -x509 -out cert.pem -keyout key.pem -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost' -extensions EXT -config <( printf "[dn]\nCN=localhost\n[req]\ndistinguished_name =   dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")`


### PHP-fpm
If you need GIT to access a private repo, add deploy keys :
 - `.docker/php-fpm/deploy_id_rsa`
 - `.docker/php-fpm/deploy_id_rsa.pub`
 and uncomment the 4 lines in `.docker/php-fpm/Dockerfile`
 
 ### Start the services
 
 `docker-compose up -d`